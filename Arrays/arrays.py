#Create Array by direct initialization 
array = ["Hello", "Happy", "World"]
print("#1 \n\t Values of array: " + str(array))

#Add elements to the an array
array = []
array.append("Hello")
array.append("Happy")
array.append("Smile")
array.append("Yay")
print("#2 \n\t Array after appending 4 elements: " + str(array))

#Remove elements by index from the array
array = ["Hello", "Sad", "Useless", "Happy", "World"]
array.pop(1)
array.pop(1)
print("#3 \n\t Array after removing 2 elements using the index: " + str(array))

#Remove elements by absolute value from the array
array = ["Hello", "Sad", "Useless", "Happy", "World"]
array.remove("Sad")
array.remove("Useless")
print("#4 \n\t Array after removing 2 elements using the value: " + str(array))

#Access one element from array
array = ["Hello", "Sad", "World"]
print("#5 \n\t Value of element 1: " + array[1])

array[1] = "Happy"
print("#5 \n\t Result after changing element 1: " + str(array))

#Get length of array
array = ["Hello", "Happy", "World"]
print("#6 \n\t Array length is: " + str(len(array)))

#Print each element from array
array = ["Hello", "Happy", "World"]
for element in array:
    print("#7 \n\t element value: " + element)

#Clear all values of array
array = ["Hello", "Happy", "World"]
array.clear()
print("#8 \n\t Array after clear: " + str(array))

#Copy the array
array = ["Hello", "Happy", "World"]
arrayNegative = array.copy()
arrayNegative[1] = "Sad"
print("#9 \n\t Values of original array: " + str(array) 
        + ", Values of copied array: " + str(arrayNegative))

#Add array to other array
array = ["Hello", "Happy", "World"]
array.extend(array)
print("#10 \n\t Doubling array values: " + str(array))

#Sort array order
array = ["Hello", "Happy", "World"]
array.sort()
print("#11 \n\t Sorting array values: " + str(array))

#Reverse array order
array = ["Hello", "Happy", "World"]
array.reverse()
print("#7 \n\t Reversed array: " + str(array))