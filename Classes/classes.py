class BinaryTreeNode:
    def __init__(self, key, value):
        self.left   = None
        self.right  = None
        self.key    = key
        self.value  = value

    def __lt__ (self, other):
        return self.key < other.key
        
    def __gt__(self, other):
        return other.__lt__(self)

class BinaryTree:
    def __init__(self, key, value):
        self.root = BinaryTreeNode(key, value)
    
    def __internalAdd(self, currentNode, key, value):
        if(currentNode == None):
            return BinaryTreeNode(key, value)
        if(key > currentNode.key):
            currentNode.left = self.__internalAdd(currentNode.left, key, value)
        if(key < currentNode.key):
            currentNode.left = self.__internalAdd(currentNode.left, key, value)
        return currentNode

    def add(self, key, value):
        self.__internalAdd(self.root, key, value)
    
    def __internalPrint(self, currentNode): 
        if(currentNode == None):
            return
        print(currentNode.value)
        self.__internalPrint(currentNode.left)
        self.__internalPrint(currentNode.right)
    
    def print(self):
        self.__internalPrint(self.root)




binaryTree = BinaryTree(1, "Hello")
binaryTree.add(0, "Happy")
binaryTree.add(2, "World")
binaryTree.print()