#1 Create a float
floatNumber = 1.0
print(floatNumber)

#2 Convert float from integer
floatNumber = float(358)
print(floatNumber)

#3 Divide float
floatNumber = 7.5 / 2.0
print(floatNumber)