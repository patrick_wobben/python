import math

#1 Create an integer
integer = 1
print("#1 - Create an integer: \n\t" + str(integer))

#2 Delete an integer
del integer
# print(integer) - NameError: name 'integer' is not defined

#3 Convert float to integer
integer = int(375.5)
print("#3 - Convert float to integer: \n\t" + str(integer))

#4 Abs of negative number
integer = abs(-100)
print("#4 - Abs of negative number: \n\t" + str(integer))

#5 Ceil a float
integer = math.ceil(9.1)
print("#5 - Ceil of float: \n\t" + str(integer))

#6 Floor a float
integer = math.floor(9.1)
print("#6 - Floor a float: \n\t" + str(integer))

#7 Minimum of integers
integer = min(10, 7, 3, 9, 1)
print("#7 - Minimum of integers: \n\t" + str(integer))

#8 Maximum of integers
integer = max(10, 7, 3, 9, 1)
print("#8 - Maximum of integers: \n\t" + str(integer))

#9 Exponential of integer
integer = math.exp(2)
print("#9 - Exponential of integer: \n\t" + str(integer))
