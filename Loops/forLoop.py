#1 Loop through list
listObject = ['Hello', 'Happy', 'World']
print('#1 Loop through list \n\t')
for itemObject in listObject:
    print(itemObject, end= ' ')
print()

#2 Loop through string
stringObject = 'HelloHappyWorld'
print('#2 Loop through string \n\t')
for character in stringObject:
    print(character + ' ', end= ' ')
print()

#3 Breaking a loop
listObject = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
print('#3 Breaking a loop \n\t')
for itemObject in listObject:
    if itemObject == 8:
        break
    print(itemObject, end= ' ')
print()

#4 Continuing a loop
listObject = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
print('#4 Continuing a loop \n\t')
for itemObject in listObject:
    if itemObject == 4:
        continue
    print(itemObject, end= ' ')
print()

#5 Loop using a range
print('#5 Loop using a range \n\t')
rangeObject = range(10)
for number in rangeObject:
    print(number, end= ' ')
print()

#6 Loop using a range in loop from index to index
print('#6 Loop using a range in loop from index to index \n\t')
for number in range(2, 7):
    print(number, end= ' ')
print()

#7 Loop using a range with different incrementation
print('#7 Loop using a range with different incrementation \n\t')
for number in range(0, 15, 2):
    print(number, end= ' ')
print()

#8 Loop using else after loop is done
print('#8 Loop using else after loop is done \n\t')
for number in range(0, 5):
    print(number, end= ' ')
else:
    print('All integers are printed!')
    
#9 Nested loop
print('#9 Nested loop \n\t')
for x in range(0, 5):
    for y in range(5, 10):
        print(x, y, end= ' ')
    print()
print()