import socket, json

class Message:
    def __init__(self, content, kind= ''):
        if issubclass(type(content), bytes):
            self.inputDataStream(content)
        else:
            self.type = kind
            self.content = content

    def inputDataStream(self, dataStream):
        jsonObject = json.loads(
            dataStream.decode('utf-8'))
        self.type = jsonObject['type']
        self.content = jsonObject['content']

    def outputDataStream(self):
        return json.dumps({
            'type' : self.type,
            'content' : self.content
        }).encode('utf-8')

# create a socket object
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 

# get local machine name
host = socket.gethostname()                           

port = 9999

# connection to hostname on the port.
s.connect((host, port))                               

# Receive no more than 1024 bytes
message = Message(s.recv(1024))                                     

s.send(json.dumps({'type': 'message', 'content': {'message': 'This is a message from the client'}}).encode('utf-8'))

s.close()
print(message.type)
print(message.content)