import socket, time, threading, json

class Repeater:
    def __init__(self, function, args= (), delay= 1):
        self.__function = function
        self.__args = args
        self.__delay = delay
        self.__stopEvent = threading.Event()
        self.__thread = threading.Thread(
            target= self.__repeat)
        self.__thread.run()

    def __repeat(self):
        while not self.__stopEvent.is_set():
            self.__function(
                self.__args)
            time.sleep(self.__delay)

class Connection:
    def __init__(self, server, client):
        self.__server = server
        self.__client = client
        self.sendMessage(Message({'message': 'Hello Happy World'}, 'start'))
        self.messageReceiveRepeater = Repeater(self.__messageReceive)
        
    def __messageReceive(self, args):
        chunk = self.__client.socket.recv(2048)
        if len(chunk) > 0:
            message = Message(chunk)
            print(message.type)
            print(message.content)

    def sendMessage(self, message):
        self.__client.socket.send(
            message.outputDataStream())

class Message:
    def __init__(self, content, kind= ''):
        if issubclass(type(content), bytes):
            self.inputDataStream(content)
        else:
            self.type = kind
            self.content = content

    def inputDataStream(self, dataStream):
        jsonObject = json.loads(
            dataStream.decode('utf-8'))
        self.type = jsonObject['type']
        self.content = jsonObject['content']

    def outputDataStream(self):
        return json.dumps({
            'type' : self.type,
            'content' : self.content
        }).encode('utf-8')

class Client:
    def __init__(self, socket, address):
        self.socket = socket
        self.address = address
        self.identifier = self.address[0] + ':' + str(self.address[1])

class Server:
    def __init__(self, port):
        self.socketInstance = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.host = (socket.gethostname(), port)
        self.connectionLimit = 5
        self.connections = {}
        self.defaultMessages = ()

    def __connectionAccept(self, args):
        print(0)
        socket, address = self.socketInstance.accept()
        client = Client(socket, address)
        print(1)
        if not client.identifier in self.connections:
            print(2)
            self.connections[client.identifier] = Connection(self, client)

    def listen(self):
        self.socketInstance.bind(self.host)
        self.socketInstance.listen(self.connectionLimit)
        
        # self.connectionAcceptRepeater = Repeater(self.__connectionAccept)
        while(True):
            self.__connectionAccept(self)
            time.sleep(1)
                
messageServer = Server(9999)
messageServer.listen()