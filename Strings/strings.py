#1 Create a string
string = "Hello Happy World"
print("#1 - Create a string: \n\t" + string)

#2 Capitalize string
string = "hello happy horld".capitalize()
print("#2 - Capitalize string: \n\t" + string)

#3 Uppercase string
string = "Hello Happy World".upper()
print("#3 - Uppercase string: \n\t" + string)

#4 Lowercase string
string = "HELLO HAPPY WORLD".lower()
print("#4 - Lowercase string: \n\t" + string)

#5 Titlecase string
string = "HELLO HAPPY WORLD".title()
print("#5 - Titlecase string: \n\t" + string)

#6 Split string
array = "HELLO HAPPY WORLD".rsplit(" ")
print("#6 - Split a string: \n\t" + str(array))

#7 Get single characters
string = "HELLO HAPPY WORLD"
print("#7 - Get a single character: \n\t" + string[0] + string[6] + string[12])

#8 Get substring of characters
string = "Hello Happy World"
print("#8 - Get a substring of characters: \n\t" + string[2:10])

#9 Strip spaces at each end of the string
string = "   Hello Happy World   "
print("#9 - Strip spaces at each end of the string: \n\t" + string.strip())

#10 Replace part of string
string = "Hello Sad World"
string = string.replace("Sad", "Happy")
print("#10 - Replace part of string: \n\t" + string)