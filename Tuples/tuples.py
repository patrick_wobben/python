#1 Create a tuple
tupleObject = ('Hello', 1, 1.107)
print("#1 - Create a tuple: \n\t" + str(tupleObject))

#2 Create a single item tuple
tupleObject = ('hello',)
print("#2 - Create a single item tuple: \n\t" + str(tupleObject))

#3 Print item of tuple
tupleObject = ('Hello', 1, 1.107)
print("#3 - Print item of tuple: \n\t" + str(tupleObject[0]))

#4 Print all items of tuple
tupleObject = ('Hello', 1, 1.107)
print("#4 - Print all items of tuple: \n\t" + str(tupleObject[0:3]))

#5 Concatenate tuples
tupleObject1 = ('Hello', 'Happy', 1)
tupleObject2 = ('World', 1, 1.107)
tupleObject = tupleObject1 + tupleObject2
print("#5 - Concatenate tuples: \n\t" + str(tupleObject))

#6 Multiply tuples
tupleObject = ('Hello', 'World') * 4
print("#6 - Multiply tuples: \n\t" + str(tupleObject))

#7 Length of tuple
tupleObject1 = ('Hello', 'Happy', 1)
print("#7 - Length of tuple: \n\t" + str(len(tupleObject)))

#8 Check if value is in tuple
tupleObject = (1, 2, 3)
print("#8 - Check if value is in tuple: \n\t" + str(2 in tupleObject))

#9 Loop through tuple
tupleObject = ('Hello', 'Happy', 'World')
print("#9 - Loop through tuple: \n\t", end = ' ')
for item in tupleObject : print(item, end = ' ')
print()

#10 Slice section from tuple
tupleObject = ('Hello', 'Happy', 'World')
print("#10 - Slice section from: \n\t" + str(tupleObject[1:]))

#11 Compare tuples
tupleObject1 = ('Hello', 'Happy', 'World')
tupleObject2 = ('Hello', 'Sad', 'World')
print("#11 - Compare tuples: \n\t" + str(tupleObject1 > tupleObject2))

#12 Min value of tuple
tupleObject = (1, 2, 3, 0)
print("#12 - Min value of tuple: \n\t" + str(min(tupleObject)))

#13 Max value of tuple
tupleObject = (4, 2, 3, 0)
print("#13 - Max value of tuple: \n\t" + str(max(tupleObject)))